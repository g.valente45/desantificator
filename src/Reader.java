/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author alext
 */
public class Reader {
	/*
	 * Selettore file
	 */
    FileReader reader = null;
    
    /**
     * Buffer 
     */
    BufferedReader streamReader = null;

    /**
     *  Estrae i santi dal file <b>lista_santi.txt</b> e restituisce
     *  la lista sottoforma di ArrayList
     * 
     * @param fileName nome file dal quale leggere i nomi dei santi
     * @return array contenente i nomi dei santi
     */
    public ArrayList<String> readSaints(String fileName) {
        ArrayList<String> listaSanti = new ArrayList<>();
        try {
            reader = new FileReader(fileName);
            streamReader = new BufferedReader(reader);
            String row = streamReader.readLine();
            while (row != null) {
                listaSanti.add(row);
                row = streamReader.readLine();
            }
            streamReader.close();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaSanti;
    }

    /**
     * Estrae gli elogi dal file <b>epiteti.txt</b> e restituisce
     * la lista sottoforma di ArrayList
     * @param fileName nome file dal quale leggere gli elogi
     * @return array contenente gli elogi
     */
    public ArrayList<String> readSentences(String fileName) {
        ArrayList<String> listaElogi = new ArrayList<>();
        try {
            reader = new FileReader(fileName);
            streamReader = new BufferedReader(reader);
            String row = streamReader.readLine();
            while (row != null) {
                listaElogi.add(row);
                row = streamReader.readLine();
            }
            streamReader.close();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaElogi;
    }
}
