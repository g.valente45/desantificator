import java.util.ArrayList;

public class SaintOperations {
	
	/**
	 * Nome file di testo contenente lista dei santi
	 */
	final String nomeFileSanti = "lista_santi.txt";
	
	/**
	 * Nome file di testo contenente la lista degli elogi
	 */
	final String nomeFileEpiteti = "epiteti.txt";
	
	/**
	 * Numero di nomine da effettuare
	 */
	int numeroNomine = -1;
	
	/**
	 * Secondi di intervallo tra un elogio ed un altro
	 */
	int intervalloElogi = -1;
	
	/**
	 * Lettore file di testo
	 */
    Reader sr;
    
    /**
     * Lista santi da nominare
     */
    ArrayList<String> listaSantiDaNominare; 
    
    /**
     * Lista elogi
     */
    ArrayList<String> listaElogi;
    
	
	/**
	 * 
	 * @param numNomine numero nomine da effettuare. Default 5 se numNomine < o = 0
	 * @param intervalloElogi secondi di intervallo tra gli elogi. Default 3 sec se intervalloElogi < o = 0
	 */
	public SaintOperations(int numNomine, int gapElogi) {
		if(numNomine > 0)
			numeroNomine = numNomine;
		else 
			numeroNomine = 5;
		
		
		if(gapElogi > 0)
			intervalloElogi = gapElogi;
		else
			intervalloElogi = 3;
		
		estraiSantiEdElogi();
	}
	
	/**
	 * 
	 * @param args parametri da cmd
	 */
	public SaintOperations(String[] args) {
		if(isArgsSet(args)) {
			intervalloElogi = Integer.parseInt(args[0]);
            numeroNomine = Integer.parseInt(args[1]);
		} else {
			intervalloElogi = 3;
			numeroNomine = 5;
		}
		estraiSantiEdElogi();
	}
	
	/**
	 * Di default, setta numeroNomine a 5 e intervalloElogi a 3
	 */
	public SaintOperations() {
		numeroNomine = 5;
		intervalloElogi = 3;
		
		estraiSantiEdElogi();
	}
	
	/**
	 * Legge e carica in memoria i nomi dei santi da nominare
	 * e gli elogi a loro dedicati
	 */
	private void estraiSantiEdElogi() {
		sr = new Reader();
		listaSantiDaNominare = sr.readSaints(nomeFileSanti);
		listaElogi = sr.readSentences(nomeFileEpiteti);
	}
	
	/**
	 * Estrae casualmente il santo dalla lista dei santi disponibili, 
	 * costruisce l'elogio all'Altissimo, elimina temporaneamente il santo per 
	 * evitare di elogiare sempre i medesimi (altrimenti qualcuno potrebbe offendersi)
	 * 
	 * @return epiteto al santo scelto
	 */
	public String getSanto() {
		String epitetoCorrente, santoCorrente;
		int indexSantoDaEstrarre = (int) ((Math.random()) * listaSantiDaNominare.size());
        int indexEpiteto = (int) ((Math.random()) * listaElogi.size());
        
        epitetoCorrente = listaElogi.get(indexEpiteto);
        santoCorrente = listaSantiDaNominare.get(indexSantoDaEstrarre);
        
        epitetoCorrente = epitetoCorrente.replace("#NomeSanto", santoCorrente);
        
        listaSantiDaNominare.remove(indexSantoDaEstrarre);        
        
		return epitetoCorrente;
	}

	/**
     * Controlla se sono stati passati parametri durante 
     * l'avvio del sw tramite command line
     * @param args parametri passati all'avvio
     * @return true se args è settato, altrimenti false
     */
    public boolean isArgsSet(String[] args) {
    	if(args.length == 2) return true; else return false;
    }
}
