/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alext 
 * 
 */
public class Main extends Thread {
	
    /**
     * @param args the command line arguments 
     */
	
    public static void main(String[] args) throws InterruptedException {
    	SaintOperations so;
    	if(args.length == 0)
    		so = new SaintOperations();
    	else 
    		so = new SaintOperations(args);
        
        for (int i = 0; i < so.numeroNomine; i++) {
            try {                
                System.err.println(so.getSanto() + "\n");                
                sleep(so.intervalloElogi * 1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}
